name := "ScaLatUniAlg"

version := "0.1"

scalaVersion in ThisBuild := "2.13.1"

libraryDependencies in Global ++= Seq(
  "org.typelevel" %% "cats-core" % "2.1.0",
  "com.chuusai" %% "shapeless" % "2.3.3",
  "org.scala-lang" % "scala-reflect" % "2.13.1"
)

scalacOptions ++= Seq("-Xfatal-warnings")
