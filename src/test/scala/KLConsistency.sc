import basic_algebra.Operation
import basic_algebra.Operation._

val homeDirectory = "/home/williamdemeo/git/lab/UNIVERSALALGEBRA/UACALC"

val B0 = Array(Array(1, 0), Array(0, 0))
val B1 = Array(Array(1, 1), Array(1, 0))

// domain and codomain parameters
val domSize = 4
val codSize = 2
val dom = 0 until domSize
val cod = 0 until codSize
val default : Int = 0 // default function value

// Parameters for (k,l)-consistency algorithm
val K = 2; val L = 3 // ...for (2,3)-consistency

println("---- maps to bin ----")

val binArrays : IndexedSeq[Array[Int]] = for {
  i <- cod; j <- cod; k <- cod; l <- cod
} yield Array(i, j, k, l)

def array2fun(f: Array[Int]): Int => Int = { x => f(x) }
def vec2fun(f: Vector[Int]): Int => Int = { x => f(x) }

val binMaps: IndexedSeq[Int => Int] = binArrays map (m => array2fun(m))

val binOps: IndexedSeq[Operation] = binMaps map (m => new Operation(m, dom, cod))

def isHom(f: Int => Int, A: Array[Array[Int]], B: Array[Array[Int]]): Boolean =
  A.indices.flatMap (i => A(i).indices.map (j => (i, j))).forall (pr => f(A(pr._1)(pr._2)) == B(f(pr._1))(f(pr._2)))

def hasHom(A: Array[Array[Int]], B: Array[Array[Int]]): Boolean = binMaps.exists(m => isHom(m, A, B))

def isPartialHom(f: Int => Int,
                 univ: IndexedSeq[Int],
                 A: Array[Array[Int]],
                 B: Array[Array[Int]]): Boolean =
  univ.flatMap (i => univ.map (j => (i, j))).forall(pr =>
    !univ.contains(A(pr._1)(pr._2)) ||
      f(A(pr._1)(pr._2)) == B(f(pr._1))(f(pr._2)))

def hasPartialHom(univ: IndexedSeq[Int],
                  A: Array[Array[Int]],
                  B: Array[Array[Int]]): Boolean =
  binMaps.exists(m => isPartialHom(m, univ, A, B))

def partialHoms(univ: IndexedSeq[Int],
                A: Array[Array[Int]],
                B: Array[Array[Int]]): IndexedSeq[Int => Int] =
  binMaps.filter(m => isPartialHom(m, univ, A, B))

println("== Generate all 4-element commutative binary tables =============================")
println("The stream of all 4-element vectors with values in {0,1,2,3}.")
lazy val strarr = (for { i <- dom; j <- dom; k <- dom; l <- dom } yield Array(i, j, k, l)).toStream

println("Stream of commutative 4 x 4 tables with no homomorphisms into B0.")
lazy val commutativeTables: Stream[Array[Array[Int]]] =
  for {
    i <- strarr; j <- strarr; k <- strarr; l <- strarr
    if i(1) == j(0) && i(2) == k(0) && i(3) == l(0) && j(2) == k(1) && j(3) == l(1) && k(3) == l(2)
    // if i(0)==1 && j(1)==0 && k(2)==3 && l(3)==2
    if !hasHom(Array(i, j, k, l), B0)
  } yield Array(i, j, k, l)

def KLHoms(ldom: IndexedSeq[Int],
           A: Array[Array[Int]],
           B: Array[Array[Int]]) =
  partialHoms(ldom, A, B).filter(p =>
    ldom.combinations(2).forall(kdom => isPartialHom(p, kdom, A, B)))

def hasAllKLHoms(A: Array[Array[Int]], B: Array[Array[Int]]): Boolean =
  dom.combinations(3).forall(ldom => KLHoms(ldom, A, B).nonEmpty)

val candidates = commutativeTables.filter(ar => hasAllKLHoms(ar, B0))
val firstFewCandidates = candidates.take(3)
val A0 = firstFewCandidates.head
val A1 = firstFewCandidates(1)
val A2 = firstFewCandidates(2)

