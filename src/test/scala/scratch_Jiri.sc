import basic_algebra.algebra_util._

  val n = 45
  val m = 3
  val N = Math.pow(2, m).toInt // cardinality of the universe

  //--parseInt(binrep: String, b: Int): Int ---------------------------
  // Converts from base b to Int.
  val x = Integer.parseInt("00101101", 2) // result: x = 45
  // (MATHEMATICA ANALOGUE: FromDigits)

  //--util.int2bin(i: Int, numPos: Int): String --------------------------
  // Converts Int to String giving binary representation w/ padding (numPos).
  // (MATHEMATICA ANALOGUE: IntegerDigits[i,2,numPos])
  // EXAMPLE: base 2 representation of n, as a string is
  val tstr = int2bin(n, 8).reverse
  //  or, as a list,
  val tlist = tstr.toList map (x => x.toString.toInt)
  //  or, as a function,
  def tfun: Int => Int = x => tlist(x)

  // To be sure, let's display the values of the function:
  println("(0 until N) map tfun = ")
  (0 until N) map tfun

  //-- processState(s: String, iters: Int): String ------------------
  // INPUT t: String representing an Int in base 2
  //       iters: Int number of iterations
  def processState(inputstate: String, t: String, iters: Int): String = {
    @scala.annotation.tailrec
    def proc_state_aux(state: String, indx: Int): String = {
      if (indx == 0) state else {
        def newstate(k: Int): Int =
          strAt(t,Integer.parseInt(strAt(state,k-1)+strAt(state, k)+strAt(state,k+1),2)).toInt
        val newstatestr: String = fun2str(newstate, 1 until (state.length-1))
        proc_state_aux(newstatestr, indx - 1)
      }
    }
    proc_state_aux(inputstate, iters)
  }

  def state_list(x: Int, y: Int, z: Int): String
    = int2bin((N * (N * x + y)) + z, 3 * m)

  //Test it
  println("state_list(0,1,0) = "); state_list(0, 1, 0)
  println("state_list(3,4,5) = "); state_list(3, 4, 5)

  val t = int2bin(n, N+2).reverse
  def t_op: (Int, Int, Int) => Int = (x, y, z) =>
    Integer.parseInt(processState(state_list(x, y, z), t, m), 2)

  //val outfun = fun2str3(t_op, 1 until N)
  val outfun = fun2arr3(t_op, 0 until N)


  // Notes:
  //
  // Mathematica Functions
  //
  //   (convert from integer to base b representation)
  //   IntegerDigits[n,b] -- gives a list of the base b digits in the integer n.
  //
  //   (convert from integer to base b representation, and pad the output)
  //   IntegerDigits[n,b,len] -- pads the list on the left with zeros to give a list of length len.
  //
  // Example: IntegerDigits[n,2,len] is binary representation of n
  //          SCALA Equivalent: int2bin(n: Int, numPos: Int)
  //
  //   (convert from base b representation to integer)
  //   FromDigits[list,b] (from decimal takes the digits to be given in base b.
  //
  // Example: FromDigits[list,2] = integer whose binary representation is list
  //          SCALA Equivalent: parseInt(list, 2)


