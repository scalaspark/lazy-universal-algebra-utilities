import org.uacalc.alg.BasicAlgebra
/**
  * @see https://www.scala-lang.org/api/2.13.0/scala/jdk/CollectionConverters$.html
  */
import scala.jdk.javaapi.CollectionConverters.asJava
import org.uacalc.io.AlgebraIO
import basic_algebra.UACalcAlgebraFactory._

val home_dir = "/home/williamdemeo/git/lab/UNIVERSALALGEBRA/UACALC"
val algebra_dir = home_dir + "/AlgebraFiles/DeMeo"

// Examples/Tests of the UACalcAlgebraFactory class/object.
println("=============================================================")
println("EXAMPLE: Build an algebra with operations defined from a (Scala) function.")
println("---- (1) the function. ----")
val plusMod5 : List[Int] => Int = l => l.sum % 5
println("---- (2) the UACalc operations built from the function. ----")
val op1: UACalcOperation =
  UACalcOpFromFun(plusMod5, "binaryPlusMod5", 2, 5)
val op2: UACalcOperation =
  UACalcOpFromFun(plusMod5, "ternaryPlusMod5", 3, 5)
println("---- (3) Check that op1([4,10]) and op2([4,10,1]) give expected results. ----")
val ans1: Int = op1.intValueAt(Array(4, 10))
println("4 + 10 mod 5 = 4 =?= " + ans1.toString)
val ans2: Int = op2.intValueAt(Array(4, 10, 1))
println("4 + 10 + 1 mod 5 = 0 =?= " + ans2.toString)
println("---- (4) Put the operations in a Java list. ----")
val my_ops = asJava(List(op1, op2))
println("---- (5) Construct the algebra. ----")
val myAlg: BasicAlgebra = new BasicAlgebra("My 1st Alg", 5, my_ops)
println("---- (6) Now apply any UACalc method that works on BasicAlgebra instances ---")
myAlg.getName()
myAlg.universe()
myAlg.con().getUniverseList
//The last result shows the algebra happens to be simple.
//println("---- (7) write the algebra to a UACalc file ----")
//AlgebraIO.writeAlgebraFile(myAlg, algebra_dir + "/UACalcAlgFromScalaFun.ua")
