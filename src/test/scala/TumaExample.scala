import org.uacalc.alg.BasicAlgebra
import scala.jdk.javaapi.CollectionConverters.asJava
import basic_algebra.UACalcAlgebraFactory._
import org.uacalc.io.AlgebraIO
import java.io.File
import java.io.PrintWriter
import basic_algebra.Tuma

object TumaExample {
  def main(args: Array[String]) {
    val home_dir = "/home/williamdemeo/git/lab/UNIVERSALALGEBRA/UACALC"
    val algebra_dir = home_dir + "/api-examples/algebras"
    val output_dir = home_dir + "/api-examples/output"

if(false) {
  val out_file = new PrintWriter(new File(output_dir + "/isSimple.txt"))
  out_file.write("Alg isSimple\n")
  out_file.write("--- --------\n")

  println("=============================================================")
  println("EXAMPLE 1b: a ternary operation defined from a (Scala) function.")
  println("            ...of interest to Jiri Tuma.")

  println("---- (1) Define a function, e.g., by specifying what it does at each point. ----")
  val n1 = 45
  val m1 = 3
  val tuma1 = new Tuma(n1, m1)
  val N1 = tuma1.cardinality // 2^m

  def t_def1(args: List[Int]): Int = tuma1.op(args(0), args(1), args(2))

  println("---- (2) Turn the function from (1) into various UACalcOperations. ----")
  val t_op1: UACalcOperation =
    UACalcOpFromFun(t_def1, "t", 3, N1)

  println("---- (3) Check that t_op([0,0,...,]) gives expected result ----")
  println("t(0,0,...,0) =?= " + t_op1.intValueAt(Array(0, 0, 0)).toString)

  println("---- (4) Make a JavaList of the operations you want as the basic operations of your algebra. ----")
  val t_uacalc_op1 = asJava(List(t_op1))

  println("---- (5) Construct the algebra. ----")
  val t_Alg1: BasicAlgebra = new BasicAlgebra("Jiri's Alg", N1, t_uacalc_op1)

  println("---- (6)(optional) Sanity check: we actually constructed something. ----")
  println("t_Alg.getName() = " + t_Alg1.getName())
  println("t_Alg.universe() = " + t_Alg1.universe())

  println("---- (7) check cardinality of congruence lattice of t_Alg ----")
  println("t_Alg.con().cardinality() = " + t_Alg1.con().cardinality())

  println("---- (8)(optional) write the algebra to a UACalc file ----")
  AlgebraIO.writeAlgebraFile(t_Alg1, "Jiris_UACalcAlgebraFromScalaFunction.ua")

  val x = "45-3"
  val A1 = AlgebraIO.writeAlgebraFile(t_Alg1, algebra_dir + "/Tuma-" + x.toString + ".ua")
}
//isCongruenceModularIdempotent(A, new ProgressReport())
    //val trials = List((45, 3), (45, 4), (45, 5))
    //trials map (p => {
    //val n = 30; val m = 5 // simple!
    //val n = 45; val m = 5 // simple!
    //val n = 106; val m = 5 // simple!
    //val n = 154; val m = 5 // simple!
    //val n = 30; val m = 6 // simple!
    //val n = 45; val m = 6 // simple!
    //val n = 106; val m = 6 // simple!
    //val n = 154; val m = 6 // simple!
    //val n = 110; val m = 6 // NOT simple
    //there are 205 congruences

    val n = 110; val m = 7
    val tuma = new Tuma(n, m)
    val N = tuma.cardinality
    def t_def(args: List[Int]): Int = tuma.op(args(0), args(1), args(2))

    // Turn the function into a UACalcOperation
    val t_op: UACalcOperation = UACalcOpFromFun(t_def, "t", 3, N)
    val t_uacalc_op = asJava(List(t_op)) // Make a JavaList of the operation
    val t_Alg: BasicAlgebra = new BasicAlgebra("Jiri's Alg" + "(" + n + "," + m + ")", N, t_uacalc_op) // Construct the algebra.

    //write the algebra to a UACalc file
    //val A = AlgebraIO.writeAlgebraFile(t_Alg, algebra_dir + "/Tuma-" + n + "-" + m + ".ua")
    // val biglist = List(0,1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,19,22,23,24,25,26,27,28,29,30,31,32,33,35,38,39,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,75,76,77,78,79,80,81,83,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127)
    val smblocks = List(10,18,20,21,34,36,37,40,41,42,43,74,82,84,85,106)
    val a = 80
    for (b <- (92 until N).filterNot(smblocks.contains)) {
      val cab = t_Alg.con().Cg(a,b, null)
      if (cab.numberOfBlocks() > 17) {
        println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv")
        println("Cg(" + a + "," + b + ") = " + cab + "  NUM BLOCKS = " + cab.numberOfBlocks())
        println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
      } else
        println("Cg(" + a + "," + b + ") = " + cab + "  num blocks = " + cab.numberOfBlocks())
    }
    for (a <- (81 until N-1).filterNot(smblocks.contains))
      for (b <- (a+1 until N).filterNot(smblocks.contains)) {
        val cab = t_Alg.con().Cg(a,b, null)
        if (cab.numberOfBlocks() > 17) {
          println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv")
          println("Cg(" + a + "," + b + ") = " + cab + "  NUM BLOCKS = " + cab.numberOfBlocks())
          println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        } else
          println("Cg(" + a + "," + b + ") = " + cab + "  num blocks = " + cab.numberOfBlocks())
      }
    //Next: test 24,52--24,127 and 50,51--50,89
    // Already tested: Cg(0,1) --> Cg(1,127) and
    // Already tested: Cg(11,12)--Cg(22,127)  (Cg(22,26) has 127 blocks)
    // Already tested: Cg(23,24) --> Cg(80,92) --> Cg(96,119)
    //val smblocks = List(10,18,20,21,42).toIndexedSeq
    //val biglist = List(0,1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,19,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63).toIndexedSeq
    //    val smblocks = List(10,18,20,21,42).toIndexedSeq
    //    for (a <- smblocks) {
    //      for (b <- 0 until 64)
    //        if (a == b) {}
    //        else {
    //          val cab = t_Alg.con().Cg(a,b, null)
    //          println("Cg(" + a + "," + b + ") = " + cab + "  num blocks = " + cab.numberOfBlocks())
    //        }
    //    }

    //println("t_Alg.con().cardinality() = " + t_Alg.con().cardinality())
    // Cg(45,53) = |0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45,53|46|47|48|49|50|51|52|54|55|56|57|58|59|60|61|62|63|64|65|66|67|68|69|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|86|87|88|89|90|91|92|93|94|95|96|97|98|99|100|101|102|103|104|105|106|107|108|109|110|111|112|113|114|115|116|117|118|119|120|121|122|123|124|125|126|127|  num blocks = 127

  }
}

