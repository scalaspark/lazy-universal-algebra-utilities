import basic_algebra.PrintableInstances.{binaryOpPrintable, booleanPrintable, format, intPrintable, stringPrintable, unaryOpPrintable}
import basic_algebra.algebra_util._

format("hello") // res14: String = 'hello '
format(true)    // res15: String = yes
val testUnOp: unaryOp[Int] = unaryOp(f = (a: Int) => a + 1, dom = 0 until 4)
format(testUnOp)
val testBinOp: binaryOp[Int] = binaryOp(f = (a: Int) => (b: Int) => a+b, dom = 0 until 4)
format(testBinOp)
def combo(a: Int, b: Int): Int = a * b
val testBinOp2: binaryOp[Int] = binaryOp(f = (a: Int) => (b: Int) => combo(a, b), dom = 0 until 4)
format(testBinOp2)
