// File: Magma.sc
// Author: williamdemeo@gmail.com
// Date: 15 Sep 2020
// Desc: tests and experiments with Magmas

import basic_algebra.algebra_util._
import basic_algebra.Magma
import basic_algebra.Magma._
import basic_algebra.PrintableInstances._ // {booleanPrintable, format, funPrintable, indexedSeqPrintable, intPrintable, iteratorFunPrintable, iteratorPrintable, magmaPrintable, optionFunPrintable, optionPrintable, pairPrintable}
import cats.Eq


implicit val eqBool: Eq[Boolean] = _ == _
implicit val eqInt: Eq[Int] = _ == _

//                                   _*_|_0__1_
// Construct the Magma with op table  0 | 1  0
//                                    1 | 0  0
val B0: Magma[Boolean] = new Magma[Boolean] {
  override val name = "B0"
  override val domain: IndexedSeq[Boolean] = IndexedSeq(false, true)
  override def binaryOp: Boolean => Boolean => Boolean = a => b => !(a || b)
}
println(format(B0))
// Construct the Magma with 0*0 = 1 = 2*3 = 3*2 and all else 0
val A0: Magma[Int] = new Magma[Int] {
  override val name = "A0"
  override val domain: IndexedSeq[Int] = 0 until 4
  override def binaryOp: Int => Int => Int = a => b => if (a == 0 && b == 0) 1 else 0
}
println(format(A0))
val A1: Magma[Int] = new Magma[Int] {
  override val name = "A1"
  override val domain: IndexedSeq[Int] = 0 until 4
  override def binaryOp: Int => Int => Int = a => b => if (a + b == 0 || a + b == 5) 1 else 0
}
println(format(A1))

println("=== Tests of Magma (dis)equalities ===============================================")
private def testMagmaEquality[A, B](mA: Magma[A])(mB: Magma[B]): Unit = {
  println("--- test: " + mA.name + " not equal " + mB.name + " ---")
  assert(!mA.equals(mB), "test failed: " + mA.name + ".equals(" + mB.name + ") should be false!")
  println("   ...test passed")
}
testMagmaEquality(A0)(B0)
testMagmaEquality(A1)(B0)
testMagmaEquality(A0)(A1)

println("=== test homomorphicAt ================================================")
def testhom(A : Magma[Int], pr: (Int, Int), b: Boolean, fin: Int => Boolean): String = {
  val msg = "test isHomomorphicAt(" + pr._1 + "," + pr._2 +")(f)(" + A.name + ", " + B0.name + ")):" +
    "\n   Magma 1: " + A.name + ", " + A.name + ".combine(" + pr._1 + ")(" + pr._2 + ") = " + A.binaryOp(pr._1)(pr._2) +
    "\n   Magma 2: " + B0.name + ", " + B0.name + ".combine(f(" + pr._1 + ")(f" + pr._2 + ")) = " + B0.binaryOp(fin(pr._1))(fin(pr._2)) +
    "\n   lhs: f(" + A.name + ".combine(" + pr._1 + ")(" + pr._2 + ")) = " + fin(A.binaryOp(pr._1)(pr._2)) +
    "\n   rhs: B0.combine(f(" + pr._1 + ")f(" + pr._2 + ")) = " + B0.binaryOp(fin(pr._1))(fin(pr._2))
  if (b) assert(isHomomorphicAt(pr._1, pr._2)(fin)(A)(B0), msg)
  else assert(!isHomomorphicAt(pr._1, pr._2)(fin)(A)(B0), msg)
  msg + "                 ...test passed" }

println("--- test 0 (homomorphicAt) ---")
val f0: Int => Boolean = x => if(x == 0) false else true
println("f0: " + format(f0, A0.domain))
for {i <- A0.domain; j <- A0.domain } yield println(testhom(A0, (i, j), b = true, f0))

val somePairs = IndexedSeq((0, 1), (1, 0), (2, 3), (3, 2))
val allPairs = A0.domain.flatMap(i => A0.domain.map(j => (i, j)))
val falsePairs = allPairs.filter(pr => {
  val i = pr._1; val j = pr._2
  !(i==j) && ( i == 0 || j == 0 || somePairs.contains((i,j)) )})

println("--- test 1 (homomorphicAt) ---")
val f1: Int => Boolean = x => if(x == 0) true else false
println("f1: " + format(f1, A1.domain))
for {i <- A1.domain; j <- A1.domain} yield println(testhom(A1, (i, j), !falsePairs.contains((i, j)), f1))

println("=== all tests of homomorphicAt passed =================================")

// domain and codomain parameters
val domSize = 4; val codSize = 2
val dom = 0 until domSize; val cod = 0 until codSize
val default : Int = 0 // default function value

// Parameters for (k,l)-consistency algorithm
val K = 2; val L = 3 // ...for (2,3)-consistency

println("--- test iteratorPrintable ------------------------------------")
val tf = IndexedSeq(false, true)
val testip2 = choose2(tf)
format(testip2)


println("--- test allMapRanges -----------------------------------------" )

val amr = allMapRanges(A0)(B0)
format(amr) // println("amr:" + amr.foldLeft("")((str, b) => str + ", " + b.toStriang()))

println("--- test allMaps -----------------------------------------" )
val am = allMaps(A0)(B0)
format(am, A0.domain)


println("== Generate all 4-element commutative binary tables =============================")
println("The stream of all 4-element vectors with values in {0,1,2,3}.")
lazy val strArr = for { i <- dom; j <- dom; k <- dom; l <- dom } yield Array(i, j, k, l)

lazy val commutativeTables = // : IndexedSeq[Array[Array[Int]]] =
  for { i <- strArr; j <- strArr; k <- strArr; l <- strArr
        if i(1) == j(0) && i(2) == k(0) && i(3) == l(0) &&
            j(2) == k(1) && j(3) == l(1) && k(3) == l(2)
        } yield Array(i, j, k, l)

// : IndexedSeq[Magma[Int]]
lazy val inputMagmas = commutativeTables.map { t =>
  new Magma[Int] {
    override val domain: IndexedSeq[Int] = dom
    override def binaryOp: Int => Int => Int = a => b => t(a)(b)
  }
}
lazy val inputMagmasWithoutHoms = inputMagmas.filter(mA => !hasHom(mA)(B0))
println("--- checking 3-consistency ---")

lazy val threeConsistentMagmasWithoutHoms: IndexedSeq[Magma[Int]] =
  //inputMagmasWithoutHoms.filter( m => kConsistentMaps(3)(m)(B0).nonEmpty )
  inputMagmas.filter( m => kConsistentMaps(3)(m)(B0).nonEmpty )

val mymagma = threeConsistentMagmasWithoutHoms.take(1)

if (false) {
  val magmaList: List[Magma[Int]] = List(0, 1, 2).map( i =>
    updateName(threeConsistentMagmasWithoutHoms(i), "A" + i.toString) )
  magmaList.foreach( m => println(format(m)) )

  def testOptionHom(m: Magma[Int], hom: Option[Int => Boolean]): String = hom match {
    case Some(f) => "--- test  (homomorphicAt) --- \nhom: " + format(f, m.domain) +
      m.domain.flatMap(i => m.domain.map(j =>
        testhom(m, (i, j), b = true, f))).foldLeft("\n")((b, s) => b + "\n" + s)
    case None => "None"
  }

  println("--- checking for homs (should all return None) ---")
  magmaList.foreach(m => println(testOptionHom(m, getHom(m)(B0))))

}
//  check commutative law
//println("Checking commutative law:")
//assert(isCommutative(AA0), "commutative law failed for " + format(AA0))
//println("commutative law holds")


//val inputMagma: Magma[Option[Int]] = (a: Option[Int], b: Option[Int]) => (a, b) match {
//  case (Some(a), Some(b)) if (a >= 0 && a < 4 && b >=0 && b < 4) => op1(a, b)
//  case (_, _) => None
//}


//"Magma: AA0
//domain: 0, 1, 2, 3,
//Cayley table:
//  .0. .1. .2. .3.
//0:  0 | 0 | 0 | 0 |
//  1:  0 | 0 | 0 | 0 |
//  2:  0 | 0 | 0 | 0 |
//  3:  0 | 0 | 0 | 0 | "
//res24: String =
//"Magma: A4
//domain: 0, 1, 2, 3,
//Cayley table:
//  .0. .1. .2. .3.
//0:  0 | 0 | 0 | 0 |
//  1:  0 | 0 | 0 | 0 |
//  2:  0 | 0 | 0 | 1 |
//  3:  0 | 0 | 1 | 0 | "
//res25: String =
//"Magma: A8
//domain: 0, 1, 2, 3,
//Cayley table:
//  .0. .1. .2. .3.
//0:  0 | 0 | 0 | 0 |
//  1:  0 | 0 | 0 | 0 |
//  2:  0 | 0 | 0 | 2 |
//  3:  0 | 0 | 2 | 0 | "
//res26: String =
//"Magma: A12
//domain: 0, 1, 2, 3,
//Cayley table:
//  .0. .1. .2. .3.
//0:  0 | 0 | 0 | 0 |
//  1:  0 | 0 | 0 | 0 |
//  2:  0 | 0 | 0 | 3 |
//  3:  0 | 0 | 3 | 0 | "
//res27: String =
//"Magma: A16
//domain: 0, 1, 2, 3,
//Cayley table:
//  .0. .1. .2. .3.
//0:  0 | 0 | 0 | 0 |
//  1:  0 | 0 | 0 | 0 |
//  2:  0 | 0 | 1 | 0 |
//  3:  0 | 0 | 0 | 0 | "
