// File: Magma.sc
// Author: williamdemeo@gmail.com
// Date: 15 Sep 2020
// Desc: tests and experiments with Magmas

import basic_algebra.algebra_util._
import basic_algebra.Magma
import basic_algebra.Magma._
import basic_algebra.PrintableInstances._ // {booleanPrintable, format, funPrintable, indexedSeqPrintable, intPrintable, iteratorFunPrintable, iteratorPrintable, magmaPrintable, optionFunPrintable, optionPrintable, pairPrintable}
import cats.Eq


implicit val eqBool: Eq[Boolean] = _ == _
implicit val eqInt: Eq[Int] = _ == _
implicit def eqIndexedSeq[A](implicit eqA: Eq[A]): Eq[IndexedSeq[A]] = (s1, s2) =>
  if (s1.isEmpty) {
    if (s2.isEmpty) true
    else false
  } else s1.indices.forall(i => eqA.eqv(s1(i), s2(i)))

implicit def eqFun[A](implicit eqA: Eq[A],
                         eqS: Eq[IndexedSeq[A]]): Eq[(A => A, IndexedSeq[A])] = (p, q) =>
  eqS.eqv(p._2, q._2) && p._2.forall(i => eqA.eqv(p._1(i), q._1(i)))

//                                   _*_|_0__1_
// Construct the Magma with op table  0 | 1  0
//                                    1 | 0  0
val B0: Magma[Boolean] = new Magma[Boolean] {
  override val name = "B0"
  override val domain: IndexedSeq[Boolean] = IndexedSeq(false, true)
  override def binaryOp: Boolean => Boolean => Boolean = a => b => !(a || b)
}
println(format(B0))
val B1: Magma[Int] = new Magma[Int] {
  override val name = "B1"
  override val domain: IndexedSeq[Int] = 0 until 2
  override def binaryOp: Int => Int => Int = a => b => (a, b) match {
    case (0, 0) => 1
    case _ => 0
  }
}
println(format(B1))

val A0ArrayOp : Array[Array[Int]] =
  Array(Array(5, 6, 7, 8, 9, 0, 1, 2, 3, 4),
        Array(6, 6, 3, 4, 2, 0, 0, 2, 3, 4),
        Array(7, 4, 7, 4, 1, 0, 1, 0, 3, 4),
        Array(8, 2, 1, 8, 1, 0, 1, 2, 0, 4),
        Array(9, 3, 3, 2, 9, 0, 1, 2, 3, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(1, 0, 1, 1, 1, 0, 1, 0, 0, 0),
        Array(2, 2, 0, 2, 2, 0, 0, 2, 0, 0),
        Array(3, 3, 3, 0, 3, 0, 0, 0, 3, 0),
        Array(4, 4, 4, 4, 0, 0, 0, 0, 0, 4))

val A0: Magma[Int] = new Magma[Int] {
  override val name: String = "A0"
  override val domain: IndexedSeq[Int] = 0 until 10
  override def binaryOp: Int => Int => Int = array2op(A0ArrayOp)
}
// (0 until 8).combinations(3).forall(m => hasPartialHom(A0)(B1)(m)) // returns false
//(0 until 8).combinations(3).find(m => !hasPartialHom(A0)(B1)(m)) // returns false


hasHom(A0)(B1) // returns false (as expected)

val ph1 = partialHoms(A0)(B1)(0 until 3) // returns true (as expected)
val ph2 = partialHoms(A0)(B1)(1 until 3) // returns true (as expected)
val ph1s = ph1.take(5).toList
//ph1s.foreach(f => println(format(f, A0.domain) + "\n"))
val ph2s: List[Int => Int] = ph2.take(5).toList
//ph2s.foreach(f => println(format(f, A0.domain) + "\n"))
format(ph1s.find(p => eqFun[Int].eqv((p, A0.domain) , (ph2s.head, A0.domain))), A0.domain)

//find(p => ph2.contains(p))
//format(ph1.toList.head, A0.domain)
//format(ph2.toList.head, A0.domain)
//hasPartialHom(A0)(B1)(2 until 5) // returns true (as expected)
//hasPartialHom(A0)(B1)(3 until 6) // returns true (as expected)
//hasPartialHom(A0)(B1)(4 until 7) // returns true (as expected)
//hasPartialHom(A0)(B1)(5 until 8) // returns true (as expected)
//val ff3 = kConsistentMaps(3)(A0)(B1)
//ff3.foreach(f => println(format(f, A0.domain) + "\n"))
//ff3.isEmpty //returns false
//val f3: List[Int => Int] = ff3.take(1).toList
//val f = f3(0)
//(0 until 8).combinations(2).foreach(s => println("(" + s(0) + ", " + s(1) + "): " + isHomomorphicAt(s(1), s(0))(f)(A0)(B1) + "\n"))


//format(f3, A0.domain)
//val ff2 = kConsistentMaps(2)(A0)(B1)
//ff2.foreach(f => println(format(f, A0.domain) + "\n"))


//val f23 = ff2.filter(ff3.contains(_))
//f23.isEmpty
//ff2.isEmpty
//val f2 = ff2.take(1)
//format(f2, A0.domain)


