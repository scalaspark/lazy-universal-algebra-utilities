// import algebra.algebra_util._
import basic_algebra.Operation
//import basic_algebra.algebra_util._
//import algebra.Operation
import basic_algebra.Operation._
import basic_algebra.Operation.distinctFuns
//import algebra.Operation_utils._

val homeDirectory = "/home/williamdemeo/git/lab/UNIVERSALALGEBRA/UACALC"

val B0 = Array(Array(1, 0), Array(0, 0))
val B1 = Array(Array(1, 1), Array(1, 0))
//val Balg = new BasicAlgebra("B0", 2,
//  seqAsJavaList(List(makeUACalcOperationFromScalaTable(B0, "*", arity2, 2))))
//AlgebraIO.writeAlgebraFile(Balg, home_dir + "/" + Balg.getName() + ".ua")

// domain and codomain parameters
val domSize = 4
val codSize = 2
val dom = 0 until domSize
val cod = 0 until codSize
val default : Int = 0 // default function value

// Parameters for (k,l)-consistency algorithm
val K = 2; val L = 3 // ...for (2,3)-consistency

println("---- maps to bin ----")

val binArrays : IndexedSeq[Array[Int]] = for {
  i <- cod; j <- cod; k <- cod; l <- cod
} yield Array(i, j, k, l)

def array2fun(f: Array[Int]): Int => Int = { x => f(x) }
def vec2fun(f: Vector[Int]): Int => Int = { x => f(x) }

val binMaps: IndexedSeq[Int => Int] = binArrays map (m => array2fun(m))

val binOps: IndexedSeq[Operation] = binMaps map (m =>
  new Operation(m, dom, cod))

def isHom(f: Int => Int, A: Array[Array[Int]], B: Array[Array[Int]]): Boolean =
  A.indices.flatMap (i => A(i).indices.map (j => (i, j))).forall (pr => f(A(pr._1)(pr._2)) == B(f(pr._1))(f(pr._2)))

def hasHom(A: Array[Array[Int]], B: Array[Array[Int]]): Boolean =
  binMaps.exists(m => isHom(m, A, B))// foldLeft(false)((b, ph) => b || ph)

def isPartialHom(f: Int => Int,
                 univ: IndexedSeq[Int],
                 A: Array[Array[Int]],
                 B: Array[Array[Int]]): Boolean =
  univ.flatMap (i => univ.map (j => (i, j))).forall(pr =>
      !univ.contains(A(pr._1)(pr._2)) ||
        f(A(pr._1)(pr._2)) == B(f(pr._1))(f(pr._2)))

def isPartialHomAsArray(f : Array[Int],
                        univ: IndexedSeq[Int],
                        A: Array[Array[Int]],
                        B: Array[Array[Int]]): Boolean = {
  if (! univ.forall(i => f.isDefinedAt(i)))
    throw new IndexOutOfBoundsException("f is not defined on all of univ")

  univ.flatMap (i => univ.map (j => (i, j))).forall( pr =>
    {
      val i = pr._1; val j = pr._2
      !univ.contains(A(i)(j)) ||
        f( A(i)(j) ) == B (f(j))(f(i))
    })
}

def hasPartialHom(univ: IndexedSeq[Int], A: Array[Array[Int]], B: Array[Array[Int]]): Boolean =
  binMaps.exists(m => isPartialHom(m, univ, A, B))

def partialHoms(univ: IndexedSeq[Int],
                A: Array[Array[Int]],
                B: Array[Array[Int]]): IndexedSeq[Int => Int] =
  binMaps.filter(m => isPartialHom(m, univ, A, B))

def partialHomsAsArrays(univ: IndexedSeq[Int],
                A: Array[Array[Int]],
                B: Array[Array[Int]]): IndexedSeq[Array[Int]] =
  binArrays.filter(m => isPartialHomAsArray(m, univ, A, B))

println("== Generate all 4-element commutative binary tables =============================")
println("The stream of all 4-element vectors with values in {0,1,2,3}.")
lazy val strarr = (for { i <- dom; j <- dom; k <- dom; l <- dom } yield Array(i, j, k, l)).toStream

println("Stream of commutative 4 x 4 tables with no homomorphisms into B0.")
lazy val commutativeTables =
  for {
    i <- strarr; j <- strarr; k <- strarr; l <- strarr
    if i(1) == j(0) && i(2) == k(0) && i(3) == l(0) && j(2) == k(1) && j(3) == l(1) && k(3) == l(2)
    // if i(0)==1 && j(1)==0 && k(2)==3 && l(3)==2
    if !hasHom(Array(i, j, k, l), B0)
  } yield Array(i, j, k, l)

def subArray(ar: Array[Array[Int]], indxs: IndexedSeq[Int]): Array[Array[Int]] = {
  val ans = Array.ofDim[Int](indxs.length, indxs.length)
  for { i <- indxs.indices; j <- indxs.indices } yield { ans(i)(j) = ar(indxs(i))(indxs(j)) }
  ans
}

def klHoms(kdom: IndexedSeq[Int],
           ldom: IndexedSeq[Int],
           A: Array[Array[Int]],
           B: Array[Array[Int]]) =
{
  val sk = partialHoms(kdom, A, B)
  val skops = sk map (h => new Operation(fun2raw(h, kdom, default), kdom, 0 until 2))

  val sl = partialHoms(ldom, A, B)
  val slops = sl map (h => new Operation(fun2raw(h, ldom, default), ldom, 0 until 2))
  val slopsRestricted = slops map (o => restrict(o, kdom))
  (skops, slops, slopsRestricted)
}

def hasKLhoms(A: Array[Array[Int]], B: Array[Array[Int]]): Boolean =
  (0 until 4).combinations(3).flatMap (l => l.combinations(2).map
       (k => (k , l))).forall (p => {
                 val (skops , _, slopsRestricted) = klHoms(p._1, p._2, A, B)
                 intersectOps(skops, slopsRestricted).nonEmpty})

val candidates = commutativeTables.filter(ar => hasKLhoms(ar, B0))
val firstFewCandidates = candidates.take(3)
val A0 = firstFewCandidates.head
val A1 = firstFewCandidates(1)
val A2 = firstFewCandidates(2)

//val A0 = Array(Array(1, 0, 0, 0), Array(0, 0, 0, 0), Array(0, 0, 0, 1), Array(0, 0, 1, 0))
val (k , l) = (Vector(0, 2), Vector(0, 1, 2))
val sk = Vector(0, 1)
val sl = Vector(0, 1, 1)

println("============================================")
println("Check that A0 hasAllKLhoms:")
hasKLhoms(A0, B0)
def getIntersecSeq(khoms : IndexedSeq[Int => Int],  k : IndexedSeq[Int],
                    lhoms: IndexedSeq[Int => Int], l : IndexedSeq[Int]) : IndexedSeq[Operation] = {
  val kops = khoms.map (m => new Operation (fun2raw (m, k), k, 0 until 2))
  val lops = lhoms.map (m => restrict (new Operation (fun2raw (m, l), l, 0 until 2), k))
  intersectOps (kops, lops)
}

val combos = (0 until 4).combinations(3).flatMap(l => l.combinations(2).map(k => (k, l)))
combos.toList.map(p => {
  val k = p._1; val l = p._2
  val khoms = partialHoms(k, A0, B0)
  val lhoms = partialHoms(l, A0, B0)

  println("============================================")
  println("(k , l): (" + k + ", " + l + ") ")
  val intersecSeq = getIntersecSeq(khoms, k, lhoms, l)
  println("--Partial homs---------------")
  if (intersecSeq.nonEmpty)
    // println("-- INTERSECTION: " + intersecSeq.map(_.toString()))
    println("-- INTERSECTION: " + intersecSeq.take(1).toString())
  else
    println("-- >>> EMPTY INTERSECTION <<< -- ")
  println("\n")

})




//val A1 = candidates(1)
//val A2 = candidates(2)

//=================================================
// Test code
if (false) {
  val A = Array(Array(1, 0, 0, 0), Array(0, 0, 0, 0), Array(0, 0, 0, 0), Array(0, 0, 0, 3))
  hasHom(A, B0)
  hasKLhoms(A, B0)
  val kdom = IndexedSeq(0, 2)
  val ldom = IndexedSeq(0, 2, 3)
  val sk = partialHoms(kdom, subArray(A, kdom), B0)
  val sl = partialHoms(ldom, subArray(A, ldom), B0)
  val intersec = intersectOps(sk map (h => new Operation(h, kdom, 0 until 2)),
    sl map (h => restrict(new Operation(h, ldom, 0 until 2), kdom)))
  intersec.isEmpty
  sk.map(f => kdom.map (x => f(x)))
  sl.map(f => ldom.map (x => f(x)))
  intersec.map(o => kdom.map(x => o.op(x)))
  //-  .map(f => IndexedSeq(0, 2).map (x => f(x))))

  //A0: Array[Array[Int]] = Array(Array(1, 0, 0, 0), Array(0, 0, 0, 0), Array(0, 0, 0, 0), Array(0, 0, 0, 3))
  //A1: Array[Array[Int]] = Array(Array(1, 0, 0, 0), Array(0, 0, 0, 0), Array(0, 0, 0, 1), Array(0, 0, 1, 0))
  //A2: Array[Array[Int]] = Array(Array(1, 0, 0, 0), Array(0, 0, 0, 0), Array(0, 0, 0, 1), Array(0, 0, 1, 3))
  val f = List(0, 0, 1, 1)
  val domf = 1 until 5
  val codf = 0 until 2
  val fop = new Operation(f, domf, codf)

  println("fop.op = " + fop.op)
  println("fop.domain = " + fop.domain)
  println("fop.codomain = " + fop.codomain)

  val g = List(0, 0, 0, 1)
  val domg = 0 until 4
  val codg = 0 until 2
  val gop = new Operation(g, domg, codg)
  val fres = restrict(fop, 1 until 4)
  println("fres.domain = " + fres.domain)
  println("fres.op = " + fres.domain.map (x => fres.op(x)))
  println("fres.domain = " + fres.domain)
  println("fres.codomain = " + fres.codomain)

  val gres = restrict(gop, 1 until 4)
  println("gres.domain = " + gres.domain)
  println("gres.op = " + gres.domain.map (x => gres.op(x)))
  println("gres.domain = " + gres.domain)
  println("gres.codomain = " + gres.codomain)

  println("test fres.domain == gres.domain...")
  assert(fres.domain == gres.domain)
  println("...test passed")

  println("test fres.codomain == gres.codomain...")
  assert(fres.codomain == gres.codomain)
  println("test passed")

  println("test that fres equals gres...")
  assert(fres.equals(gres))
  println("test passed")

  println("intersection of (fop, fres) and (gres, gop) is " + intersectOps(IndexedSeq(fop, fres), IndexedSeq(gres, gop)))
  val A0 = Array(Array(0, 1, 2, 3), Array(4, 5, 6, 7), Array(8, 9, 10, 11), Array(12, 13, 14, 15))
  val A0sub = subArray(A0, 1 until 3) // should be Array(Array(5, 6), Array(9, 10))
}

