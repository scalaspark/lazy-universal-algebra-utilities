package basic_algebra

abstract class Nat {
    def isZero: Boolean
    def predecessor: Nat
    def successor = Succ(this)
    def + (that: Nat): Nat
    def - (that: Nat): Nat
    def maxnat(that : Nat) : Nat
    def minnat(that : Nat) : Nat
}

object Zero extends Nat {
    def isZero = true
    def predecessor = throw new Error("0.predecessor")
    // def successor = new Succ(this)  (moved to superclass)
    def +(that: Nat): Nat = that
    def -(that: Nat) = if (that.isZero) this else throw new Error("0-negative")
    def maxnat(that : Nat): Nat = that
    def minnat(that : Nat) = this
}

case class Succ(n: Nat) extends Nat {
    def isZero = false
    def predecessor: Nat = n
    // def successor = new Succ(this) (moved to superclass)
    def +(that: Nat): Nat = Succ(n + that)
    def -(that: Nat) = if (that.isZero) this else n - that.predecessor
    def maxnat(that: Nat) =   (this, that) match {
        case (_, Zero) => this
        case (Succ(m), Succ(n)) => m maxnat n
        case (_, _) => this
    }
    def minnat(that: Nat) = if (that.isZero) that else this.predecessor minnat that.predecessor
}


abstract class Fin {
    def isEmpty : Boolean
    def bound : Nat
    //def succ = new fsucc(this)
    def +(that: Fin) : Fin
    def *(that: Fin) : Fin
//    def max(that: Fin) : Nat
}

object fzero extends Fin {
    def isEmpty = true
    def bound = Zero
    def +(that: Fin): Fin = that
    def *(that: Fin) = this
//    def max(that: Fin) = that.bound
}

case class fsucc (n: Nat) extends Fin {
    def isEmpty = false
    def bound = n
    def +(that: Fin) = fsucc(this.bound maxnat that.bound)
    def *(that: Fin) = fsucc(this.bound minnat that.bound)
}

