// File: Printable.scala
// Author: williamdemeo@gmail.com
// Date: 15 Sep 2020
// Refs: https://www.scalawithcats.com/dist/scala-with-cats.html#exercise-printable-library
//       https://www.scalawithcats.com/dist/scala-with-cats.html#contravariant

package basic_algebra

import basic_algebra.algebra_util.{binaryOp, dropDomain, unaryOp}

//============== Formatting Output ==========================
trait Printable[A] { self =>
  def format(value: A): String
  def contraMap[B](f: B => A): Printable[B] =
    (value: B) => self.format(f(value))
}

object PrintableInstances {
  def format[A](a: A)(implicit p: Printable[A]): String = p.format(a)

  // For testing purposes, here are some instances of Printable for String, Boolean, and Int:
  implicit val stringPrintable: Printable[String] = (str: String) => s"'$str'"
  implicit val booleanPrintable: Printable[Boolean] = (b: Boolean) => if (b) "true" else "false"
  implicit val intPrintable: Printable[Int] = (i: Int) => i.toString
  implicit val intListPrintable: Printable[List[Int]] = (l: List[Int]) =>
    l.foldLeft("")((b, i) => b + ", " + i.toString)

  implicit def pairPrintable[A, B](implicit pa: Printable[A], pb: Printable[B]): Printable[(A, B)] = pr =>
    "(" + pa.format(pr._1) + ", " + pb.format(pr._2) + ")"

  implicit def listPrintable[A](implicit p: Printable[A]): Printable[List[A]] = (l: List[A]) =>
    l.foldRight("")((a, b) => format(a) + ", " + b)

  implicit def indexedSeqPrintable[A](implicit p: Printable[A]): Printable[IndexedSeq[A]] =
    (l: IndexedSeq[A]) => l.foldRight("")((a, b) => format(a) + ", " + b)

  implicit def iteratorPrintable[A](implicit p: Printable[A]): Printable[Iterator[A]] =
    (l: Iterator[A]) => l.foldRight("")((a, b) => format(a) + ", " + b)

  implicit def iteratorIndexedSeqPrintable[A](implicit p: Printable[A],
                                              pi: Printable[IndexedSeq[A]]): Printable[Iterator[IndexedSeq[A]]] =
    (l: Iterator[IndexedSeq[A]]) => l.foldRight("")((ia, b) => format(ia) + ", " + b)

  implicit def unaryOpPrintable[A](implicit p: Printable[A]): Printable[unaryOp[A]] = (opA : unaryOp[A]) =>
    opA.dom.foldRight("")((a, b) => "(" + format(a) + ", " + format(opA.f(a)) + "), " + b)

  implicit def funPrintable[A, B](implicit pb: Printable[B]): Printable[(A => B, IndexedSeq[A])] =
    (pr: (A => B, IndexedSeq[A])) =>  pr._2.foldRight("")((a, b) => pb.format(pr._1(a)) + ", " + b)

  implicit def iteratorFunPrintable[A, B]
   (implicit fab: Printable[(A => B, IndexedSeq[A])]): Printable[(Iterator[A => B], IndexedSeq[A])] =
    (pr: (Iterator[A => B], IndexedSeq[A])) => pr._1.foldRight("")((f, b) => fab.format(f, pr._2) + ", " + b)

  implicit def binaryOpPrintable[A](implicit p: Printable[A]): Printable[binaryOp[A]] = (opA : binaryOp[A]) => {
    val str1 = opA.dom.foldLeft("   ")((b, i) => b + "." + format(i) + ". ")
    val str2 = opA.dom.foldLeft("")((b, i) => b + "\n" + format(i) + ":  " +
      opA.dom.foldLeft("")((b, j) => b + format(opA.f(i)(j)) + " | "))
    str1 + str2
  }

  implicit def magmaPrintable[A](implicit p: Printable[A]): Printable[Magma[A]] =
    (mA: Magma[A]) => "Magma: " + mA.name + "\ndomain: " + format(mA.domain) + "\nCayley table:\n" +
      format(binaryOp((a: A) => (b: A) => mA.binaryOp(a)(b), mA.domain))

  // Here's another example where we use contramap to define the implicit Printable.
  // First, we define a new container class called Box.
  final case class Box[A](value: A)
  // Then, we create an instance from an existing instance using contramap.
  implicit def boxPrintable[A](implicit p: Printable[A]): Printable[Box[A]] =
    p.contraMap[Box[A]](_.value)   // (pretty cool)

  implicit def optionPrintable[A](implicit p: Printable[A]): Printable[Option[A]] =
    p.contraMap[Option[A]] {
      case Some(a) => a
      case None => throw new IllegalArgumentException
    }

  implicit def optionFunPrintable[A, B](implicit
                                        pf: Printable[(A => B, IndexedSeq[A])],
                                        pb: Printable[B]): Printable[(Option[A => B], IndexedSeq[A])] =
    (pr: (Option[A => B], IndexedSeq[A])) =>
      pr._2.foldRight("")((a, b) => format(dropDomain(pr._1)(a)) + ", " + b)

  // This works as follows:
  // format(Box("hello world"))   // res: String = 'hello world'
  // format(Box(true))            // res: String = yes
  // format(Box(123))             // res: String = 123
}
