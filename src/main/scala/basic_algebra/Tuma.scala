package basic_algebra
import algebra_util._
class Tuma (n: Int, m: Int) {
  private val N = Math.pow(2, m).toInt
  def cardinality: Int = N

  /** Process state
    * @param inputstate
    * @param t a String representing an Int in base 2
    * @param iters an Int number of iterations
    */
  def processState(inputstate: String, t: String, iters: Int): String = {
    @scala.annotation.tailrec
    def proc_state_aux(state: String, indx: Int): String = {
      if (indx == 0) state else {
        def newstate(k: Int): Int =
          strAt(t,Integer.parseInt(strAt(state,k-1)+strAt(state, k)+strAt(state,k+1),2)).toInt
        val newstatestr: String = fun2str(newstate, 1 until (state.length-1))
        proc_state_aux(newstatestr, indx - 1)
      }
    }
    proc_state_aux(inputstate, iters)
  }

  def state_list(x: Int, y: Int, z: Int): String =
    int2bin((N * (N * x + y)) + z, 3 * m)

  val t : String = int2bin(n, 8).reverse

  def op: (Int, Int, Int) => Int = (x, y, z) =>
    Integer.parseInt(processState(state_list(x, y, z), t, m), 2)
}
