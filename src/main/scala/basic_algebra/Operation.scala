package basic_algebra

import scala.annotation.tailrec

class Op(val opRaw: Int => Int, val domain: IndexedSeq[Int], val codomain: IndexedSeq[Int]) {
  // op throws exception if evaluated outside domain or output is outside codomain
  def op: Int => Option[Int] = x =>
    if (!domain.contains(x) || !codomain.contains(opRaw(x))) None else Some(opRaw(x))

  // Operation objects are equal if they have the same domain, codomain, and are extensionally equal.
  override def equals(that: Any): Boolean = that match {
      case fop: Op => domain == fop.domain && codomain == fop.codomain && Operation.extEqOp(op, fop.op, domain)
      case _ => false
    }
  override def toString : String = {
    "  opRaw   : " + (0 to domain.max).map(i => opRaw(i)) + "\n" +
    "  domain  : " + domain.indices.map(i => domain(i)) + "\n" +
    "  codomain: " + codomain.indices.map(i => codomain(i)) + "\n" +
    "  op      : " + domain.foldLeft[String](" ")((b , i) => b + "(" + i + ", " + op(i) + ")")
  }
}

class Operation(val opRaw: Int => Int, val domain: IndexedSeq[Int], val codomain: IndexedSeq[Int]) {
  // op throws exception if evaluated outside domain or output is outside codomain
  def op: Int => Int = x => {
    if (!domain.contains(x))
      throw new IllegalArgumentException(x + " is not in the domain: " + domain)
    val ans = opRaw(x)

    if (!codomain.contains(ans))
      throw new IllegalArgumentException(ans + " is not in the codomain: " + codomain)
    ans
  }
  // Operation objects are equal if they have the same domain, codomain, and are extensionally equal.
  override def equals(that: Any): Boolean = {
    that match {
      case fop: Operation => domain == fop.domain && codomain == fop.codomain && Operation.extEq(op, fop.op, domain)
      case _ => false
    }
  }
  override def toString : String = {
    "  opRaw   : " + (0 to domain.max).map(i => opRaw(i)) + "\n" +
    "  domain  : " + domain.indices.map(i => domain(i)) + "\n" +
    "  codomain: " + codomain.indices.map(i => codomain(i)) + "\n" +
    "  op      : " + domain.foldLeft[String](" ")((b , i) => b + "(" + i + ", " + op(i) + ")")
  }
}

object Operation{
  // Functions are extensionally equal if they give same value for each input.
  def extEq(op1 : Int => Int, op2 : Int => Int, dom :IndexedSeq[Int]) : Boolean =
    dom.forall (i => op1(i) == op2(i))

  def extEqOp(op1 : Int => Option[Int], op2 : Int => Option[Int], dom :IndexedSeq[Int]) : Boolean =
    dom.forall (i => op1(i) == op2(i))

  def isSubset(a : IndexedSeq[Int], b : IndexedSeq[Int]) : Boolean =
    a.forall(p => b.contains(p))

  def extContains(v : IndexedSeq[Int => Int], f : Int => Int, dom : IndexedSeq[Int]) : Boolean =
    v.exists(g => dom.forall (x => g(x) == f(x)))

  // This takes a range of function values defines a function having these values on the given domain
  // and `default` outside of domain.
  def fun2raw(v : Int => Int, dom : IndexedSeq[Int], default : Int = 0) : Int => Int = x => {
      if (dom.contains(x)) v(x)
      else default
    }

  def seq2raw(v : IndexedSeq[Int], dom : IndexedSeq[Int], default : Int = 0) : Int => Int = {
    if (! dom.indices.forall(i => v.isDefinedAt(dom(i))) ) throw new IllegalArgumentException("v not defined on all of dom")
    x => {
      if (dom.contains(x)) v(x)
      else default
    }
  }
  def fun2string(f: Int => Int, dom : IndexedSeq[Int], name: String = "f") : String =
    name + dom.indices.foldLeft[String](": ")((b , i) => b + f(i) + ", ")

  def distinctFuns(v : IndexedSeq[Int => Int], dom: IndexedSeq[Int]) : IndexedSeq[Int => Int] = {
    @tailrec
    def distinctAux(acc: IndexedSeq[Int => Int],
                    v: IndexedSeq[Int => Int], dom : IndexedSeq[Int]): IndexedSeq[Int => Int] =
      v match {
        case IndexedSeq() => acc
        case h +: t =>
          if (extContains(acc, h, dom)) distinctAux(acc, t, dom) else distinctAux(acc :+ h, t, dom)
      }
    distinctAux(IndexedSeq(), v, dom)
  }

  def intersectOps(i1: IndexedSeq[Operation], i2: IndexedSeq[Operation]): IndexedSeq[Operation] =
    i1.filter(p => i2.contains(p))

  def itersectionIndices(i1: IndexedSeq[Operation], i2: IndexedSeq[Operation]): IndexedSeq[Int] = {
    val intersec = intersectOps(i1, i2)
    intersec.map(p => i2.indexOf(p))
  }

  def restrict(fop: Operation, newDomain: IndexedSeq[Int]): Operation = {
    if (! isSubset (newDomain, fop.domain) )
      throw new IllegalArgumentException("newDomain is not a subset of domain: " + fop.domain)
    //val f: Int => Int = newDomain.indices map (i => fop.opRaw(newDomain(i))) // <-- This line caused the bug.
    new Operation(fop.opRaw, newDomain, fop.codomain)
  }

}

