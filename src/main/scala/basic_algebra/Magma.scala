// File: Magma.scala
// Author: williamdemeo@gmail.com
// Date: 15 Sep 2020

package basic_algebra

import cats.Eq
import cats.kernel.Eq._


//=============== Magmas ================================
trait Magma[A] {
  val name : String = "default name"
  val domain: IndexedSeq[A]
  def binaryOp: A => A => A
}
case class LiftMagma[A](mA: Magma[A])
                       (restrictedDomain: IndexedSeq[A]) extends Magma[Option[A]] {
  override val name: String = mA.name
  override val domain : IndexedSeq[Option[A]] =
    mA.domain.map { a =>
      if (restrictedDomain.contains(a)) Some(a) else None
    }

  override def binaryOp: Option[A] => Option[A] => Option[A] =
    a => b => (a, b) match {
      case (Some(a), Some(b))
        if restrictedDomain.contains(a) && restrictedDomain.contains(b) &&
          restrictedDomain.contains(mA.binaryOp(a)(b)) => Some(mA.binaryOp(a)(b))
      case (_, _) => None
    }
}

object Magma {
  def apply[A](implicit magma: Magma[A]): Magma[A] = magma
  def equality[A](implicit e: Eq[A]): Eq[A] = e
  def magmaEquality[A](mA: Magma[A], mB: Magma[A]): Boolean =
    mA.domain == mB.domain && mA.binaryOp == mB.binaryOp

  implicit def eqOpt[B](implicit eqB: Eq[B]) : Eq[Option[B]] = {
    case (Some(b1), Some(b2)) => eqB.eqv(b1, b2)
    case (None, _) => true
    case (_, None) => true
    case _ => false
  }

  def commutativeLaw[A](x: A, y: A) (m: Magma[A]) (implicit eA: Eq[A]): Boolean =
    equality(eA).eqv(m.binaryOp(x)(y), m.binaryOp(y)(x))

  def isCommutative[A](mA: Magma[A])(implicit e: Eq[A]): Boolean =
    mA.domain.flatMap( i => mA.domain.map( j => (i, j) ) ).
      forall( pr => commutativeLaw(pr._1, pr._2)(mA) )

  /**
    * Return true if f and the binary op are compatible at (x, y).
    * @param x arbitrary value from carrier of magma mA
    * @param y arbitrary value from carrier of magma mA
    * @param f an arbitrary map from carrier type of mA to carrier type of mB
    * @param mA an arbitrary domain over type A
    * @param mB an arbitrary magma over type B
    * @param eB implicit equality of values in codomain
    * @tparam A domain magma carrier type
    * @tparam B codomain magma carrier type
    * @return true if f and the binary op are compatible at (x, y).
    */
  def isHomomorphicAt[A, B](x: A, y: A)
                           (f: A => B)
                           (mA: Magma[A])
                           (mB: Magma[B])
                           (implicit eB: Eq[B]): Boolean =
    eqv( f(mA.binaryOp(x)(y)), mB.binaryOp(f(x))(f(y)) )

  def isHomomorphic[A, B](f : A => B)
                         (mA: Magma[A])
                         (mB: Magma[B])
                         (implicit eB: Eq[B]): Boolean =
    mA.domain.flatMap( i => mA.domain.map( j => (i, j) ) ).
      forall( pr => isHomomorphicAt(pr._1, pr._2)(f)(mA)(mB) )

  def isPartialHom[A, B](f: A => B)
                        (mA: Magma[A])
                        (mB: Magma[B])
                        (subDom: IndexedSeq[A])
                        (implicit eB: Eq[B]) : Boolean =
    isHomomorphic[Option[A], Option[B]] (liftFunc(f)) (LiftMagma(mA)(subDom)) (LiftMagma(mB)(mB.domain))

  def hasPartialHom[A, B](mA: Magma[A])
                         (mB: Magma[B])
                         (subDom: IndexedSeq[A])
                         (implicit eB: Eq[B]): Boolean =
    allMaps(mA)(mB).exists(f => isPartialHom(f)(mA)(mB)(subDom))

  def partialHoms[A, B](mA: Magma[A])
                       (mB: Magma[B])
                       (subDom: IndexedSeq[A])
                       (implicit eB: Eq[B]): Iterator[A => B] =
    allMaps(mA)(mB).filter( f => isPartialHom(f)(mA)(mB)(subDom) )


  // combinations with repetitions allowed
  def comb[A](as: IndexedSeq[A], k: Int): Iterator[IndexedSeq[A]] =
    IndexedSeq.fill(k)(as).flatten.combinations(k)

  /**
    * The collection of all maps from the domain of A to the domain of B
    * @param mA an arbitrary magma over type A
    * @param mB an arbitrary magma over type B
    * @tparam A type of carrier of magma mA
    * @tparam B type of carrier of magma mB
    * @return all maps from the domain of A to the domain of B
    */
  def allMapRanges[A, B](mA: Magma[A])(mB: Magma[B]): Iterator[Iterator[IndexedSeq[B]]] =
    comb(mB.domain, mA.domain.length).map (_.permutations)

  def allMaps[A, B](mA: Magma[A])(mB: Magma[B]) : Iterator[A => B] =
    allMapRanges(mA)(mB).flatten.map(m => (a: A) => m(mA.domain.indexOf(a)))

  def hasHom[A, B](mA: Magma[A])(mB: Magma[B])(implicit eB: Eq[B]): Boolean =
    allMaps(mA)(mB).exists(f => isHomomorphic(f)(mA)(mB))

  def getHom[A, B](mA: Magma[A])(mB: Magma[B])(implicit eB: Eq[B]): Option[A => B] =
    allMaps(mA)(mB).find(f => isHomomorphic(f)(mA)(mB))

  def hasHom2[A, B](mA: Magma[A])(mB: Magma[B])(implicit eB: Eq[B]): Boolean =
    allMapRanges(mA)(mB).exists(_ exists (m =>
      isHomomorphic((a: A) => m(mA.domain.indexOf(a)))(mA)(mB)))

  def kConsistentMaps[A, B](k: Int)
                           (mA: Magma[A])
                           (mB: Magma[B])
                           (implicit eB: Eq[B]): Iterator[A => B] =
    allMaps(mA)(mB).filter( f => mA.domain.combinations(k).
                                 forall(sd => isPartialHom(f)(mA)(mB)(sd)) )


  def updateName[A](mA: Magma[A], newName: String): Magma[A] = new Magma[A] {
    override def binaryOp: A => A => A = mA.binaryOp
    override val domain: IndexedSeq[A] = mA.domain
    override val name: String = newName
  }

  def liftFuncCod[A, B](f: A => B)(dom: IndexedSeq[A], cod: IndexedSeq[B]): A => Option[B] =
    a => if (dom.contains(a) && cod.contains(f(a))) Some(f(a)) else None

  def liftFuncRestricted[A, B](f: A => B)
                              (dom: IndexedSeq[A], cod: IndexedSeq[B]): Option[A] => Option[B] = {
    case Some(a) if dom.contains(a) && cod.contains(f(a)) => Some(f(a))
    case _ => None
  }

  def liftFunc[A, B](f: A => B): Option[A] => Option[B] = {
    case Some(a) => Some(f(a))
    case _ => None
  }


}
