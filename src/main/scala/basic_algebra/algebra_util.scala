// File: algebra_util.scala
// Author: williamdemeo@gmail.com
// Date: 15 Sep 2020

package basic_algebra
import cats._
import cats.implicits._

object algebra_util {

  // Convert Int to String giving the binary representation, w/ optional padding (numPos)
  def int2bin(i: Int, numPos: Int): String = {
    @scala.annotation.tailrec
    def nextPow2(i: Int, acc: Int): Int =
      if (i < acc) acc
      else nextPow2(i, 2 * acc)

    val nextpow = nextPow2(i, math.pow(2, numPos).toInt) + i
    nextpow.toBinaryString.substring(1)
  } // This is like IntegerDigits[i,2,numPos] in Mathematica.

  // Convert Array of Arrays of Ints into binary operation: Int => Int => Int
  def array2op(ar: Array[Array[Int]]): Int => Int => Int = {i => j => ar(i)(j)}

  //Convert vals of unary fun `f` to String (for inputs `inseq`)
  def fun2str(f: Int => Int, inseq: IndexedSeq[Int]): String =
    inseq.foldLeft[String]("")((acc: String, x) => acc + f(x).toString)

  //e.g., fun2str(tfun, (0 until N)

  //Express vals of unary fun `f` as List[Int] (for inputs `inseq`)
  def fun2lst(f: Int => Int, inseq: IndexedSeq[Int]): List[Int] =
    (inseq map (x => f(x))).toList

  //e.g. fun2lst(tfun, 0 until N)

  //Express vals of unary fun `f` as List[Int] (for inputs `inseq`)
  def fun2seq(f: Int => Int, inseq: IndexedSeq[Int]): IndexedSeq[Int] =
    inseq map ((x: Int) => f(x))

  //e.g. fun2seq(tfun, 0 until N)

  //Express vals of binary fun `f(x,y)` as Array[Array[Int]] (x, y range over `inseq)`.
  def fun2seq2(f: (Int, Int) => Int, inseq: IndexedSeq[Int]): IndexedSeq[IndexedSeq[Int]] =
    inseq map (i => inseq map (j => f(i, j)))

  //e.g.def bin_tfun : (Int, Int) => Int = (x, y) => tfun(x)
  //fun2seq2(bin_tfun, (0 until N))

  //Express vals of binary fun `f(x,y)` as Array[Array[Int]] (x, y range over `inseq)`.
  def fun2arr2(f: (Int, Int) => Int, inseq: IndexedSeq[Int]): Array[Array[Int]] =
    (inseq map (i => (inseq map (j => f(i, j))).toArray)).toArray

  //e.g. fun2arr2(bin_tfun, (0 until N))

  //Express vals of binary fun `f(x,y)` as List[String] (x, y range over `inseq)`.
  def fun2str2(f: (Int, Int) => Int, inseq: IndexedSeq[Int]): List[String] =
    (inseq flatMap (i => inseq map (j => f(i, j).toString))).toList

  //e.g., fun2str2(bin_tfun, (0 until N))

  def fun2str3(f: (Int, Int, Int) => Int, inseq: IndexedSeq[Int]): List[List[String]] =
    (inseq map (x => fun2str2((y, z) => f(x, y, z), inseq))).toList

  def fun2arr3(f: (Int, Int, Int) => Int, inseq: IndexedSeq[Int]): Array[Array[Array[Int]]] =
    (inseq map (i => (inseq map (j => (inseq map (k => f(i, j, k))).toArray)).toArray)).toArray

  //(inseq map (x => fun2arr2((y, z) => f(x, y, z), inseq))).toArray

  //e.g., def tern_tfun : (Int, Int, Int) => Int = (x, y, z) => bin_tfun(x,y)
  // fun2str3 (tern_tfun, (0 until N))

  //character at index k, as a String
  def strAt(s: String, k: Int): String = s.charAt(k).toString

  // ========== Equality =======================
  val eqInt: Eq[Int] = Eq[Int]
  val eqString: Eq[String] = Eq[String]
  //We can use eqInt directly to test for equality:
  eqInt.eqv(123, 123) // res1: Boolean = true
  eqInt.eqv(123, 231) // res2: Boolean = false

  // Unlike Scala's == method, if we try to compare objects of different types using eqv we get a compile error:
  //eqInt.eqv(123, "234") // error: type mismatch;

  //We can also import the interface syntax in cats.syntax.eq to use the === and =!= methods:
  // (moved above)
  123 === 123 // res2: Boolean = true
  123 =!= 231 // res3: Boolean = true


  def getCombineInt(f: (Int, Int) => Int, dom: List[Int]): String =
    dom.foldLeft("")((b, i) => b + dom.foldLeft("\n")((b2, j) => b2 + ", " + f(i, j).toString))

  def getCombineBool(f: (Boolean, Boolean) => Boolean, dom: List[Boolean]): String =
    dom.foldLeft("")((b, i) => b + dom.foldLeft("\n")((b2, j) => b2 + ", " + f(i, j).toString))

  case class unaryIntOp(f: Int => Int, dom: IndexedSeq[Int])
  case class binaryIntOp(f: Int => Int => Int, dom: IndexedSeq[Int])
  //final case class unaryOp[A](f: A => A, )
  case class unaryOp[A](f: A => A, dom: IndexedSeq[A])
  case class binaryOp[A](f: A => A => A, dom: IndexedSeq[A])

  def choose2[A](s: IndexedSeq[A]): IndexedSeq[(A, A)] =
    for {
      i <- s
      j <- s
    } yield {(i, j)}

  def dropDomain[A, B](f: Option[A => B]) : A => Option[B] = a => f match {
    case Some(f) => Some(f(a))
    case None => None
  }


}
