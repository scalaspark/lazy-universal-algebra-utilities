package basic_algebra

import org.uacalc.alg.op.{AbstractOperation, OperationSymbol}
import org.uacalc.alg.op.Operations.makeBinaryIntOperation

object UACalcAlgebraFactory {

  // type alias since Scala has its own Operation class
  type UACalcOperation = org.uacalc.alg.op.Operation

  // type alias to point out that util.List is the Java representation of a list
  type JavaList[T] = java.util.List[T]

  /** UACalcOpFromUnaryFun
    * Return a subclass of AbstractOperation with intValueAt() defined using the function fn.
    *
    * @param fn       the function used to define the intValueAt() method of the class.
    * @param op_name  a string name for the class.
    * @param arity    the arity of the operation (must be 1 if fn is a list or tuple)
    * @param algSize  the size of the universe of the algebra on which this operates.
    *
    * @example
    * 1. Define a scala function, e.g., addition mod 5.
    * {{{
    *  def plusMod5(args: List[Int]): Int = {
    *    def plusMod5_aux(xs: List[Int], acc: Int): Int = xs match {
    *      case Nil => acc % 5
    *      case y :: ys => plus_mod5_aux(ys, acc + y)
    *    }
    *    plusMod5_aux(args, 0)
    *  }
    * }}}
    * 2. Use the operation defined in 1 to make some UACalcOperations.
    * {{{
    *  val op1: UACalcOperation =
    *    UACalcOpFromUnaryFun(plusMod5, "binaryPlusMod5", 2, 5)
    *
    *  val op2: UACalcOperation =
    *    UACalcOpFromUnaryFun(plusMod5, "ternaryPlusMod5", 3, 5)
    * }}}
    *
    * 3. Check that the op1([4,10]) and op2([4,10, 1]) work as expected.
    * {{{
    *  val ans1: Int = op1.intValueAt(Array(4,10))
    *  println("4 + 10 mod 5 = " + ans1.toString)
    *  val ans2: Int = op2.intValueAt(Array(4,10,1))
    *  println("4 + 10 + 1 mod 5 = " + ans2.toString)
    * }}}
    *
    * 4. Make a JavaList of the operations from 2 that you want for the basic operations of your algebra.
    * {{{
    *  val my_ops = asJava(List(op1, op2))
    * }}}
    *
    * 5. Build a UACalcAlgebra.
    * {{{
    *  val myAlg: BasicAlgebra = new BasicAlgebra("My Alg", 5, my_ops)
    * }}}
    */
  def UACalcOpFromFun(fn      : List[Int] => Int,
                      op_name : String,
                      arity   : Int,
                      algSize : Int ): UACalcOperation =
    new AbstractOperation(op_name, arity, algSize) {
      override def intValueAt(args: Array[Int]): Int = fn(args.toList)
      override def valueAt(list: JavaList[_]): AnyRef = ???
    }

  /** UACalcOpFromList
    * Construct a UACalc operation from a Scala list of ints.
    * @param fn list of ints
    * @param op_name name of the operation (op symbol)
    * @param arity arity of the operation
    * @param algSize size of the algebra
    * @return new UACalc AbstractOperation
    */
  def UACalcOpFromList(fn      : List[Int],
                       op_name : String,
                       arity   : Int,
                       algSize : Int ): UACalcOperation =
    new AbstractOperation(op_name, arity, algSize) {
      override def intValueAt(arg: Int): Int = fn(arg)
      override def valueAt(list: JavaList[_]): AnyRef = ???
    }

  /** UACalcOpFromArrayOfArrays
    * Construct a UACalc operation from a Scala Array of Arrays of Ints.
    * @param fn array of arrays of ints
    * @param op_name name of operation
    * @param arity arity of operation
    * @param algSize size of algebra
    * @return UACalcOperation
    */
  def UACalcOpFromArrayOfArrays(fn      : Array[Array[Int]],
                                op_name : String,
                                arity   : Int,
                                algSize : Int ): UACalcOperation =
    makeBinaryIntOperation(new OperationSymbol(op_name, arity),algSize,fn)

  /**
    * Construct a UACalc operation from an integer-valued function of arity |A|
    * @param fn arbitrary function of type (A => Int) => Int,
    * @param embedding (of A into Int)
    * @param op_name name of operation
    * @param arity (of operation)
    * @param algSize size of the algebra
    * @tparam A
    * @return AbstractOperation
    */

  def UACalcOpFromGeneralFun[A](fn : (A => Int) => Int,
                                embedding: A => Int,
                                op_name : String,
                                arity   : Int,
                                algSize : Int ): UACalcOperation =
    new AbstractOperation(op_name, arity, algSize) {
      override def intValueAt(args: Array[Int]): Int = fn(ArrayToTuple(args, embedding))
      override def valueAt(list: JavaList[_]): AnyRef = ???
    }

  def ArrayToTuple[A](ar : Array[Int], embedding: A => Int): A => Int =
    a => ar(embedding(a))
}
